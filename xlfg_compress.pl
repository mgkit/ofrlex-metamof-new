#!/usr/bin/env perl

## try to collapse several entries for a same lemma using more underspecified macros

use strict;

use AppConfig qw/:argcount :expand/;


my $config = AppConfig->new(
			    "templates|t=f" => {DEFAULT => "dico.xlfg.templates"}
			   );
$config->args();

my $templates = $config->templates;

-f $templates
  or die "missing template file $templates";

my $fsets = {};
my $value2fset = {};
my $macros = {};
my $map = {};


open(T,"<",$templates)
  or die "can't process template file $templates: $!";
while (<T>) {
  chomp;
  if (/^\s+(\w+)\s*=\s*\{(.+?)\}\s*;/) {
    my $fset = $1;
    my $values = $2;
    $values =~ s/\s+//og;
    my @values = split(/,/,$values);
    # print STDERR "*** register fset $fset <@values>\n";
    foreach my $v (@values) {
      $value2fset->{$v}{$fset} = 1;
      $fsets->{$fset}{$v} = 1;
    }
    next;
  }
  while (/(\@[\w.]+)/og) {
    # print STDERR "*** register macro $1\n";
    $macros->{$1} = 1;
  }
  if (/^\@(\w+)\s*=\s*\[\s*(\w+)\s*=\s*(\w+)\s*\]\s*;/) {
    print STDERR "*** register redirect $1 => $3 [for fset $2]\n";
    $map->{$1} = $3;
  }
}
close(T);


sub value_redirect {
  my $v = shift;
  return $map->{$v} || $v;
}

my @last = ();

while (<>) {
  chomp;
  my ($form,$score,$cat,$info) = split(/\t+/,$_);
  my ($lemma) = $info =~ /pred="(.*)",/;
  my $key = "$form $cat $lemma";
  if (!@last || $last[-1]{key} eq $key) {
    push(@last, { key => $key, info => $info, entry => $_});
    next;
  } else {
    # emit last
    analyze_and_emit(@last);
    # reset
    @last = ();
    push(@last, { key => $key, info => $info, entry => $_});
  }
}

sub get_macro {
  my $m = shift;
  return '@'.join('.',map {join('_',@$_)} @$m);
}

my $missing_macros = {};

sub analyze_and_emit {
  my @data = @_;
  foreach my $data (@data) {
    my ($macro) = $data->{info} =~ /,\@([\w.]+?)\]/;
    $data->{macro} = [map {[$_]} split(/\./,$macro)];
    # print "** Found entry $data->{key} macro=@{$data->{macro}}\n";
  }
  ## reduce the number of entries
  while (1) {
    my @new = ();
    my @exclude = ();
    foreach my $e1 (@data) {
      (grep {$_ == $e1} @exclude) and next;
      foreach my $e2 (@data) {	
	(grep {$_ == $e2} @exclude) and next;
	($e1->{entry} eq $e2->{entry}) and next;
	my $new_macro = try_compact_macro($e1->{macro},$e2->{macro});
	if ($new_macro) {
	  my $m1 = get_macro($e1->{macro});
	  my $m2 = get_macro($e2->{macro});
	  my $m = get_macro($new_macro);
	  if ($m ne $m1 and $m ne $m2) {
	    print STDERR "*** {$e1->{key}} reduce macro m1=$m1 m2=$m2 => new=$m\n";
	    unless (exists $macros->{$m} && !exists $missing_macros->{$m}) {
	      print STDERR "*** macro to be defined $m\n";
	      my @def = ();
	      foreach my $c (@$new_macro) {
		my @c = @$c;
		if (scalar(@c) == 1) {
		  my $mc = '@'.$c[0];
		  (exists $macros->{$mc}) or print STDERR "*** missing single macro $mc : $c[0]\n";
		  push(@def,$mc);
		} else {
		  my $mc = '@'.join('_',@c);
		  unless (exists $macros->{$mc} || exists $missing_macros->{$mc}) {
		    foreach my $c1 (@c) {
		      my $mc1 = '@'.$c1;
		      (exists $macros->{$mc1}) or print STDERR "*** missing single macro $mc1 : $c1\n";
		    }
		    my $fset = compatible(@c);
		    my $vs = join(' | ',map {value_redirect($_)} @c);
		    print STDERR "DEFINE $mc = [ $fset = $vs ];\n";
		    $missing_macros->{$mc} = [$c];
		  }
		  push(@def,$mc);
		}
	      }
	      unless (exists $missing_macros->{$m}) {
		my $vs = join(', ',@def);
		print STDERR "DEFINE $m = [ $vs ];\n";
		$missing_macros->{$m} = $new_macro;
	      }
	    }
	    $e1->{macro} = $new_macro;
	    $e1->{entry} =~ s/\@([\w.]+?)\]/$m]/;
	    push(@exclude,$e2);
	    last;
	  }
	}
      }
      push(@new,$e1);
    }
    @exclude or last;
    @data = @new;
  }
  ## generate new macros
  foreach my $data (@data) {
    print "$data->{entry}\n";
  }
}

sub try_compact_macro {
  my ($m1,$m2) = @_;
  my @m1 = @$m1;
  my @m2 = @$m2;
  scalar(@m1) == scalar(@m2) or return 0;
  scalar(@m1) or return 0;
  my $modifs = 0;
  my $l = scalar(@m1) - 1;
  my @new = ();
  foreach my $i (0 .. $l) {
    (compatible(@{$m1[$i]}) && compatible(@{$m2[$i]})) or return 0;
    if (macro_same($m1[$i],$m2[$i])) {
      push(@new,$m1[$i]);
    } elsif ($modifs) {
      #  no more than one modif allowed
      return 0;
    } elsif (my $fset = compatible(@{$m1[$i]},@{$m2[$i]})) {
      push(@new, macro_combine($fset,$m1[$i],$m2[$i]));
      $modifs = 1;
    } else {
      return 0;
    }
  }
  return \@new;
}

sub macro_same {
  my ($m1,$m2) = @_;
  scalar(@$m1) == scalar(@$m2) or return 0;
  my $l = scalar(@$m1) - 1;
  foreach my $i (0 .. $l) {
    $m1->[$i] eq $m2->[$i] or return 0;
  }
  return 1;
}

sub macro_combine {
  my ($fset,$m1,$m2) = @_;
  my @new = @$m1;
  foreach my $c (@$m2) {
    (grep {$c eq $_} @new) or push(@new,$c);
  }
  @new = sort {$a cmp $b} @new;
  return \@new;
}

sub compatible {
  my $lfsets = {};
  foreach my $v (@_) {
    my $v2 = value_redirect($v);
    exists $value2fset->{$v2} or next;
    foreach my $fset (keys %{$value2fset->{$v2}}) {
      $lfsets->{$fset} ++;
    }
  }
  my $l = scalar(@_);
  foreach my $fset (keys %$lfsets) {
    ($lfsets->{$fset} == $l) and return $fset;
  }
  return 0;
}
