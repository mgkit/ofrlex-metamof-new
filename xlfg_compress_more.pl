#!/usr/bin/env perl

use strict;

use AppConfig qw/:argcount :expand/;


my $config = AppConfig->new(
                           );
$config->args();

my $keep = {
	    faire => 1,
	    vouloir => 1,
	    sembler => 1,
	    devoir => 1,
	    pouvoir => 1,
	    dire => 1,
	    laisser => 1,
	    estre => 1,
	    avoir => 1
	   };

my $seen = {};

my $last = undef;

while (<>) {
  chomp;
  my ($form,$score,$cat,$info) = split(/\t+/,$_);
  my $key = "${form}__${cat}";
  my $simple = $_;
  $simple =~ s/\w+___\d+__\d+//og;
  if (defined $last && $key ne $last->{key}) {
    # emit last
    foreach my $entries (values %{$last->{entries}}) {
      analyze_and_emit(@$entries);
    }
    # reset
    $last = undef;
  }  
  my ($lemma,$subcat) = /"(\S+?)__\S+?<(.+?)>/;
  my @subcat = map {[split(/:/,$_)]} split(/,/,$subcat);
  $simple =~ s/<.*?>//og;
  $last ||= {key => $key, entries => {}};
  push(@{$last->{entries}{$simple}}, {entry => $_, subcat => [@subcat], lemma => $lemma });
}

if (defined $last) {
  foreach my $entries (values %{$last->{entries}}) {
    analyze_and_emit(@$entries);
  }
}

sub analyze_and_emit {
  my @data = @_;
  @data = sort {scalar(@{$a->{subcat}}) <=> scalar(@{$b->{subcat}})} @data;
  while (@data) {
    my $e = shift @data;
    my $subcat = $e->{subcat};
    if (!exists $keep->{$e->{lemma}} && grep {is_subsumed($subcat,$_->{subcat})} @data) {
      print STDERR "## deleted $e->{entry}\n";
      next;
    }
    print "$e->{entry}\n";
  }
}

sub is_subsumed {
  my ($s1,$s2) = @_;
  my @s1 = @$s1;
  my @s2 = @$s2;
  scalar(@s1) > scalar(@s2) and return 0;
  while (@s1) {
    my $arg1 = shift @s1;
    my $arg2 = shift @s2;
    ($arg1->[0] eq $arg2->[0]) or return 0;
    ($arg1->[1] eq $arg2->[1]) or return 0;
  }
  my $xs1 = str($s1);
  my $xs2 = str($s2);
  print STDERR "## subsumed <$xs1> by <$xs2>\n";
  return 1;
}

sub str {
  my $s = shift;
  return join(",",map {"$_->[0]:$_->[1]"} @$s);
}
